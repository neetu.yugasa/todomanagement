from fastapi import FastAPI

from app.server.routes.todo import router as TodoRouter

app = FastAPI()

app.include_router(TodoRouter, tags=["Todo"], prefix="/todo")


@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "Welcome to the TODO Management app !"}
