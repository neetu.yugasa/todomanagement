from fastapi import APIRouter, Body
from fastapi.encoders import jsonable_encoder

from app.server.database import (
    add_todo,
    delete_todo,
    retrieve_todo,
    retrieve_todos,
    update_todo,
)
from app.server.models.todo import (
    ErrorResponseModel,
    ResponseModel,
    todoSchema,
    UpdatetodoModel,
)

router = APIRouter()


@router.post("/", response_description="todo data added into the database")
async def add_todo_data(todo: todoSchema = Body(...)):
    todo = jsonable_encoder(todo)
    new_todo = await add_todo(todo)
    return ResponseModel(new_todo, "todo added successfully.")


@router.get("/", response_description="todos retrieved")
async def get_todos():
    todos = await retrieve_todos()
    if todos:
        return ResponseModel(todos, "todos data retrieved successfully")
    return ResponseModel(todos, "Empty list returned")


@router.get("/{id}", response_description="todo data retrieved")
async def get_todo_data(id):
    todo = await retrieve_todo(id)
    if todo:
        return ResponseModel(todo, "todo data retrieved successfully")
    return ErrorResponseModel("An error occurred.", 404, "todo doesn't exist.")


@router.put("/{id}")
async def update_todo_data(id: str, req: UpdatetodoModel = Body(...)):
    req = {k: v for k, v in req.dict().items() if v is not None}
    updated_todo = await update_todo(id, req)
    if updated_todo:
        return ResponseModel(
            "todo with ID: {} name update is successful".format(id),
            "todo name updated successfully",
        )
    return ErrorResponseModel(
        "An error occurred",
        404,
        "There was an error updating the todo data.",
    )


@router.delete("/{id}", response_description="todo data deleted from the database")
async def delete_todo_data(id: str):
    deleted_todo = await delete_todo(id)
    if deleted_todo:
        return ResponseModel(
            "todo with ID: {} removed".format(id), "todo deleted successfully"
        )
    return ErrorResponseModel(
        "An error occurred", 404, "todo with id {0} doesn't exist".format(id)
    )
