from typing import Optional

from pydantic import BaseModel, Field


class todoSchema(BaseModel):
    title: str = Field(...)
    description: str = Field(...)
   

    class Config:
        schema_extra = {
            "example": {
                "title": "create api",
                "description": "should be done by day",
                
            }
        }


class UpdatetodoModel(BaseModel):
    title: Optional[str]
    description: Optional[str]
  

    class Config:
        schema_extra = {
            "example": {
                "title": "create api",
                "description": "should be done by day",
            }
        }


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}
